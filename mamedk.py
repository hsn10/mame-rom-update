#! /usr/bin/python
import re
import httplib
import base64
import socket
import sys

#HTML Searching
SEARCH1='<b style="bigstdtext"><a href="http://www.mame.dk(.*?)">'
SEARCH2='a href="(.*?)">click'
	
# stuff for http://www.mame.dk/ ROM archive

LOGINURL='/login.phtml?comefrom=/'
MAMEDK='www.mame.dk'
DOWNLOAD='/download/rom/'
USERAGENT='Mozilla/3.0 (X11; U; Linux 1.2.13 i386)'
COOKIE=None

def getlogincookie(auth=''):

    if len(auth)==0:
	return None

    if COOKIE is not None:
        return COOKIE;

    ht=httplib.HTTP(MAMEDK)
    ht.putrequest('GET',LOGINURL)
    ht.putheader('Host',MAMEDK)
    ht.putheader('Accept','*/*')
    ht.putheader('Authorization','Basic %s' % base64.encodestring(auth).strip())
    ht.endheaders()

    reply,msg,hdrs = ht.getreply()

    if reply!=200:
	return None
   
    result=re.search('(.*); expires=',hdrs.getheader('set-cookie'))

    if result==None:
	return None

    return result.group(1)

def get(gamename,login=''):
    global COOKIE
    try:
	#set up login cookie first
	if len(login)>0:
	    if COOKIE==None:
		COOKIE=getlogincookie(login)
		if COOKIE!=None:
		    sys.stderr.write("Successfully logged to mame.dk by cookie: "+COOKIE+"\n");
		else:
		    sys.stderr.write("Mame.dk Login incorrect!\n")
		    COOKIE="login=failed"
	    
	ht=httplib.HTTP(MAMEDK)
	ht.putrequest('GET',DOWNLOAD+gamename+'/'+gamename+'/')
        ht.putheader('User-Agent',USERAGENT)
	ht.putheader('Host',MAMEDK)
        ht.putheader('Accept','image/*, text/*')
	if COOKIE!=None:
	    ht.putheader('Cookie',COOKIE)
	ht.endheaders()
	
        reply,msg,hdrs = ht.getreply()
	if reply!=200: return None

	f=ht.getfile()
	result=re.search(SEARCH1,f.read(20000))
	f.close()
	if result==None:
	    return None
	
	#2nd HTML
	ht=httplib.HTTP(MAMEDK)
	ht.putrequest('GET',result.group(1))
        ht.putheader('User-Agent',USERAGENT)
	ht.putheader('Host',MAMEDK)
        ht.putheader('Accept','image/*, text/*')
	if COOKIE!=None:
	    ht.putheader('Cookie',COOKIE)
	ht.endheaders()

        reply,msg,hdrs = ht.getreply()
	if reply!=200: return None
	
	f=ht.getfile()
	result=re.search(SEARCH2,f.read(20000))
	f.close()
	if result==None:
		return None
	url=result.group(1)
	return url
    except socket.error:
	sys.stderr.write("Network error when communicating with mame.dk.\n")
	return None

if __name__ == "__main__":
    if len(sys.argv)<2:
	sys.stderr.write("Usage: python "+sys.argv[0]+" <gamename>\n")
    else:
        execfile("config.py",globals())
	for i in sys.argv[1:]:
	    url=get(i,LOGIN)
	    if url==None:
	         sys.stderr.write("Error getting url for game "+i+" from mame.dk.\n")
	    else:
		 print url
