#! /usr/bin/python

from partzip import partzip,zipcache
import os
import romsite
import cPickle

def savecache(cache):
    if cache.dirty==1:
	c=open(ZIPCACHE,'w')
	cPickle.dump(cache,c)
	c.close()
    
def load(filename,targetdir='./'):
    try:
       f=open(filename,'r')
    except IOError:
       return
    #init ZIPcache
    try:
       c=open(ZIPCACHE,'r')
       cache=cPickle.load(c)
       c.close()
    except Exception:
       cache=zipcache()
       
    cache.checkVersion()
    skip=0
    oldgame=''
    for l in f.readlines():
        data=l.split()
        if (len(data)<4): continue
        if data[0]!=oldgame:
            game=data[0]
	    oldgame=game
	    urls=None
            #mame jiz nahrane .zip file v adresari?
	    if os.path.exists(targetdir+game+".zip"):
		skip=1
                continue
	    else:
                skip=0
        else:
            if skip==1: continue

        file,size,crc=data[1],data[2],eval('0x'+data[3])

        #have we needed file in game subdirectory?
        
	if not os.path.exists(targetdir+game+'/'+file):
            if urls==None:
                print "Locating game",game,"on",SITENAME
                urls=romsite.get(game,SITENAME,LOGIN)
                if urls==None:
                    print "  !Unable to locate game",game,"on",SITENAME
                    skip=1
                    continue
            #We have some possible URLs with ZIPs for this game
	    for url in urls[:]:
                pz=partzip(url[0],cache,url[1:])
                print "  Found",url[0],"with size",pz.size(),"B"
                if pz.size()<=0:
                    #some err
		    print "Zip archive file access error."
		    urls.remove(url)
                    continue
                if pz.size()<SMALLSIZE:
                    #too small load entire zip file
                    print "  File is small, loading entire zip."
                    data=pz.part()
                    if len(data)==pz.size():
                        z=open(targetdir+game+'.zip','wb')
                        z.write(data)
                        z.close()
                        print "  File sucessfully loaded and saved."
                    skip=1
                    break;
                #load part of Zip file
                print "loading",file,"from",game
	        if SEARCHZIPSBYNAME==0:
		    data=pz.getfile(file,size,crc)
	        else:
		    data=pz.getfile(file)
                savecache(cache)
                if data!=None:
                    #save it to file
                   try:
                      os.makedirs(targetdir+game)
                   except OSError:
                      pass
                   z=open(targetdir+game+'/'+file,'wb')
                   z.write(data)
                   z.close()
                   print " ... got it"
		   break
    savecache(cache)
execfile("config.py",globals())
if __name__ == "__main__":
   load(LOADQUEUE,LOADED)
