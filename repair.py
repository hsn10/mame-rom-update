#! /usr/bin/python
import verify
from sys import argv;
import os

if __name__ == "__main__":
    if len(argv)==1: #no arguments
        if os.path.exists(verify.LOADQUEUE):
            games=verify.initstate()
	    verify.repairloadedroms(verify.ROMPATHS,verify.LOADED,games,verify.WANTMERGE,verify.FIXED,verify.CREATEROMSWITHSUBDIRS)
	else:
	     verify.repair()
	    
    else: #command line argument - familyname
        games=verify.initstate()
	for z in argv[1:]:
	      verify.repairfamily(z,verify.ROMPATHS,verify.LOADED,games,verify.WANTMERGE,verify.FIXED,verify.CREATEROMSWITHSUBDIRS)
