#! /usr/bin/python
# Generate merged CLRMamePro datafile

import verify

if __name__== "__main__":
   games=verify.initstate(0)
   for rom in verify.getmasters(games):
     print "game ("
     print "\tname",rom
     family=verify.getfamily(rom,games)
     roms=verify.listfamilyroms(family,games)
     for file in roms:
       print "\trom ( name",file[1],"size",file[2],"crc",hex(file[3])[2:],")"
     print ")\n"
