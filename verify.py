#! /usr/bin/python

from zipfile import *
from string import lower,rfind
import cPickle
import os
from game import game
import sys
import warnings

warnings.filterwarnings("ignore")

#read output of mame -verifysets command
def readverify(file,result={}):
    try:
        f=open(file,'r')
    except IOError:
        return result
    for line in f.xreadlines():
        data=line.split()
        if data[0]!='romset':
            continue
        if result.has_key(data[1]):
            newgame=result[data[1]]
        else:
            newgame=game(data[1])
            print newgame
        if data[2]!='is':
            newgame.cloneof=data[2][1:-1]
            data=data[4]
        else:
            data=data[3]

        if data=='bad': newgame.good=game.BAD
        elif data=='good': newgame.good=game.PERFECT
        elif data=='best': newgame.good=game.BESTAVAIL
        result[newgame.name]=newgame
    f.close()
    return result

#read file in clr mame pro format
# for example mame output
def readinfo(file,games={},fullload=1):
    try:
        f=open(file,'r')
    except IOError:
        return games
    skip=0
    gm=None
    for line in f.xreadlines():
        data=line.split()
        if len(data)==0: continue
        if data[0]==')':
            gm=None
            skip=0
            continue
        if gm==None and data[1]=='(':
            gm=game('==PLACEHOLDER==')
            if data[0]!='game' and data[0]!='resource':
                # print "SKIP SECTION",data
                skip=1
                continue
        if skip==1:
            # print "SKIP DATA",data
            continue
        if data[0]=='name':
            if fullload==0 and not games.has_key(data[1]):
                skip=1
                gm=None
                continue
            else:
                gm=game(data[1])
                games[data[1]]=gm
                skip=0
        if data[0]=='cloneof':
            gm.cloneof=data[1]
            continue
        if data[0]=='rom':
            if data[4]=='merge':
                gm.needmerge=1
                continue
            else:
	        if data[7]=='nodump': data[7]='0'
		try:
                  gm.roms.append( (data[3],int(data[5]),eval('+0x'+data[7]) ))
		except Exception:
		  print "Don't know how to parse:",data

    f.close()
    return games

#najde soubor filename v adresarich
def locatefile(filename,dirs):
    if filename==None: return None
    for d in dirs:
        if os.path.exists(d+filename):
           return d+filename
    return None

#najde vsechny vyskyty souboru filename v dirs
def locatefiles(filename,dirs):
    if filename==None: return None
    result=[]
    for d in dirs:
        if os.path.exists(d+filename):
           result.append(d+filename)
    return result

#vraci seznam ZipFile objektu k urcite hre
def locatezips(game,dirs):
    result=[]
    for f in locatefiles(game+'.zip',dirs):
       zf=openzip(f)
       if zf!=None:
           result.append(zf)
    return result

#otevre zipsoubor filename, vraci objekt ZipFile
def openzip(filename):
    if filename==None: return None
    zip=None
    try:
      zip=ZipFile(filename,'r') 
    except IOError:
        pass
    except BadZipfile:
        print "BADZIPFILE",filename
        pass #should be thrown up the stack and handled
    except RuntimeError:
        print "BADZIPFILE",filename
    return zip

#vraci prvni nalezene ZipFile objekt pro danou hru. Fallback to master set.
def zipforgame(game,romdirs,games={}):
    file=locatefile(game+'.zip',romdirs)
    if file==None:
        if not games.has_key(game): return None # neznama hra
        if games[game].cloneof==None: return None # master set
        file=locatefile(games[game].cloneof+'.zip',romdirs)
    return openzip(file)
        
#prekontroluje pozadovanou romku v zipaci
#vstup name,size,crc + archiv, kde se to hleda
def verifyrominzip(zip,name,size,crc=0):
  if zip==None:
     return game.MISSING

  for zi in zip.infolist():
     if zi.file_size==size:
        if crc!=0:
           if zi.CRC==crc:
              if lower(zi.filename)[-len(name):]==lower(name):
                  return game.PERFECT
              else:
                  return game.RENAME
        else:
           if lower(zi.filename)[-len(name):]==lower(name):
              return game.BESTAVAIL

  return game.BAD


def verifyfamily(gamename,romdirs,games):
    for i in getfamily(gamename,games):
        writeresult(i,verifygame(i,romdirs,games))

#returns list of missing rom,size,crc
def verifygame(gamename,romdirs,games):
    if not games.has_key(gamename):
        print "Game",gamename,"is not known"
        return None
    zip=zipforgame(gamename,romdirs,games)
    if zip==None:
     #   print "[ERROR] Can not find zipfile for game",gamename
        games[gamename].good=game.MISSING
        return None

    sys.stderr.write("Verifying game "+gamename+"\r")
    gameroms=games[gamename].roms;
    found=0
    bad=game.PERFECT
    result=[]
    test=0
    for rom in gameroms:
        #find ROM in ZIPfile
        name,size,crc=rom
        test=verifyrominzip(zip,name,size,crc)
        if crc==0 and test<0:
            test=game.BESTAVAIL #unknown ROM can be missing, ignore it
        if test< bad:
            bad=test
        if test<0:
            result.append( ( name, size, crc) )
        elif crc!=0:
              found=1
    zip.close()
    if found==0:
        games[gamename].good=game.MISSING
        return None

    games[gamename].good=bad
    return result

#udela verify na bad or unknown roms
def verifybadroms(romdir,games={}):
    pchecked={}
    f=open(LOADQUEUE,'w')
    f.close()
    for game in games.values():
        if game.good>0: continue
        writeresult(game.name,verifygame(game.name,romdir,games))
        pchecked[game.name]=1
        if game.needmerge==1 and game.cloneof!=None and not pchecked.has_key(game.name):
            writeresult(game.name,verifygame(game.cloneof,romdir,games))
            pchecked[game.cloneof]=1

def writeresult(game,result):
    if result!=None and len(result)>0:
        f=open(LOADQUEUE,'a');
        for i in result:
           f.write(game+" "+i[0]+" "+repr(i[1])+" "+hex(i[2])[2:]+"\n")
        f.close()

#vraci master rom sety
def getmasters(games):
    result=[]
    for game in games.values():
        if game.cloneof==None:
            result.append(game.name)
    return result

#vraci seznam nazvu her z jedne rodiny
def getfamily(name,games):
    result=[]
    if games[name].cloneof!=None:
        name=games[name].cloneof
    result.append(name)
    
    for game in games.values():
        if game.cloneof==name:
            result.append(game.name)
    return result       

#vraci seznam romek ze seznamu her
def listfamilyroms(familymembers,games):
    #romname,filename,size,crc
    result=[]
    for name in familymembers:
        game=games[name]
        for rom in game.roms:
           result.append( (name,rom[0],rom[1],rom[2]))
    return result

#odstrani duplicitni filename z family rom listu
def removeduplicatesfromfamily(familylist):
    fnames={}
    result=[]
    for polozka in familylist:
       if not fnames.has_key(polozka[1]):
          result.append(polozka)
          fnames[polozka[1]]="We have it"
       else:
          pass
          #print "Duplicate fname",polozka[1]
          
    return result

#Vstup ZIPINFO OBJEKT, ROMFAMILY LIST
#vraci seznam (romsetname,filename)
def findfileinfamily(zi,family):
    result=[] 
    for i in family:
        rn,fn,sz,crc=i
        if zi.file_size==sz:
            if crc!=0:
                if zi.CRC==crc:
                    result.append( (rn,fn) )
            else:
              if lower(zi.filename)[-len(fn):]==lower(fn):
                    result.append( (rn,fn) )
    return result

#zipfile,familyromslist,loaded direct
#neodstrauje z roms listu
def extractzip(zip,roms,loaded,unknown=None,subdirs=1):
    if zip==None:
        return
    for zi in zip.infolist():
        if zi.file_size==0:
            continue
        foundfiles=findfileinfamily(zi,roms)
        if len(foundfiles)==0: # neznamy objekt
            if unknown==None:
                continue
            try:
                os.makedirs(unknown)
            except OSError:
                pass
            #zjisti zda se unknown name neshoduje nahodou se jmenem romky
            target=lower(os.path.basename(zi.filename))
            if subdirs==0:
               clash=0
               for i in roms:
                 if target==i[1]:
                    print "Clashing filename for unknown file",target
                    clash=1
                    break
               if clash==1:
                   target="r-"+target

            #najdi kam se ma neznamy objekt rozpakovat          
            target=unknown+target
            if os.path.exists(target):
                for i in range(ord('a'),ord('z')):
                    target=target[:-1]+chr(i)
                    if not os.path.exists(target):
                        break
                else:
                    print "ERROR: Can not find suitable filename!"
                    continue # nenalezena  vhodna lokace
            foundfiles.append ( (None,target) )    
        for rn,fn in foundfiles:
            if rn!=None:
               target=loaded+rn+'/'+fn
            else:
               target=fn
               rn=''
               
            #make directory
            try:
                os.makedirs(loaded+rn)
            except OSError:
                pass

            if os.path.exists(target):
                continue
            print "Extracting",zi.filename,"to",target
            try:
              f=open(target,'wb')
              f.write(zip.read(zi.filename))
              f.close()
            except Exception:
                try:
                    os.unlink(target)
                except OSError:
                    pass
    zip.close()    

def getunknowndir(master,rn,mergeit,subdirs):
        if subdirs==0 and mergeit==1:
            baddir='unknown/'
        elif mergeit==1:
            baddir='unknown/'+rn+'/'
        else:
            baddir=rn+'/unknown/'
        return baddir

def repairloadedroms(romdirs,loaded,games,mergeit=1,targetdir='./',subdirs=0):
    need={}
    f=open(LOADQUEUE,'r')
    for line in f.readlines():
        z=line.split()
        if len(z)==0: continue
        if games[z[0]].cloneof!=None:
           z[0]=games[z[0]].cloneof
        need[z[0]]="repairneeded"
    f.close()
    print len(need.keys()),"loaded sets will be repaired."
    for g in need.keys():
        repairfamily(g,romdirs,loaded,games,mergeit,targetdir,subdirs)

#opravi vsechny vadne romky
def repairbadroms(romdirs,loaded,games,mergeit=1,targetdir='./'):
    for master in getmasters(games):
        bad=0
        for rn in getfamily(master,games):
            st=games[rn].good
            if st>game.MISSING and st<game.BESTAVAIL and st!=game.UNKNOWN:
                bad=1
                break
        if bad==1:
            print "reparing family",master
            repairfamily(master,romdirs,loaded,games,mergeit,targetdir,CREATEROMSWITHSUBDIRS)

#opravi celou rodinku romek
def repairfamily(name,zromdirs,loaded,games,mergeit=1,targetdir='./',subdirs=0):
    if not games.has_key(name):
        print "No information about",name,"Can not repair."
        return None
    if games[name].cloneof!=None:
        master=games[name].cloneof
    else:
        master=name
    #extract everything good from every loaded rom into loaded directory

    family=getfamily(master,games)
    familyroms=listfamilyroms(family,games)
    if mergeit==1:
       nodupfamilyroms=removeduplicatesfromfamily(familyroms)
    else:
       nodupfamilyroms=familyroms
   
    romdirs=[loaded]+zromdirs
    foundsomething=0
    #step 1 - prohledat a rozpakovat romky
    for rn in family:
        for zf in locatezips(rn,romdirs):
          extractzip(zf,nodupfamilyroms,loaded,loaded+getunknowndir(master,rn,mergeit,subdirs),subdirs)
          foundsomething=1#nasli jsme nejake zipy
        #delete extracted archives
        if KEEPORIGINALROMS==0:
            for ar in locatefiles(rn+'.zip',romdirs):
                try:
                    os.unlink(ar)
                except OSError:
                    print "[ERROR] Can not delete",ar
    
    if foundsomething==0:
        return
    #zip it and we are done
    cd=os.getcwd()
    if mergeit==1:
        try:
           os.makedirs(targetdir)
        except OSError:
           pass
        zip=ZipFile(targetdir+master+".zip","w",ZIP_DEFLATED)
        print "Zipping family",master
	fnames={}
        for rn,fn,sz,crc in familyroms:
            if os.path.exists(loaded+rn+"/"+fn):
                if subdirs==0:
                   sz=fn
                else:
                   sz=rn+"/"+fn
                #add it!
                if not fnames.has_key(fn):
		   zip.write(loaded+rn+"/"+fn,sz)
		   fnames[fn]="haveit"
                os.unlink(loaded+rn+"/"+fn)
        #add files from unknown dirs
        for rn in family:
            #clean empty dirs
            try:
                os.rmdir(loaded+rn)
            except OSError:
                pass
            bd=loaded+getunknowndir(master,rn,mergeit,subdirs)
            if not os.path.exists(bd): continue
            for fn in os.listdir(bd):
               if subdirs==0:
                  sz=fn
               else:
                  sz="unknown/"+rn+"/"+fn
               zip.write(bd+fn,sz)
               os.unlink(bd+fn)
            os.rmdir(bd)
        zip.close()
        print "... done."
        os.chdir(cd)
    else:
        #generate unmerged sets
        try:
            os.makedirs(targetdir)
        except OSError:
            pass
        for rn in family:
            if not os.path.exists(loaded+rn): continue
            zip=ZipFile(targetdir+rn+".zip","w",ZIP_DEFLATED)
            print "Zipping set",rn
            for gn,fn,sz,crc in familyroms:
                if gn!=rn: continue
                if os.path.exists(loaded+rn+"/"+fn):
                    #add it!
                    zip.write(loaded+rn+"/"+fn,fn)
                    os.unlink(loaded+rn+"/"+fn)
            #add unkown files
            bd=loaded+getunknowndir(master,rn,mergeit,subdirs)
            if os.path.exists(bd):
                for fn in os.listdir(bd):
                   if subdirs==0:
                      sz=fn
                   else:
                      sz="unknown/"+fn
                   zip.write(bd+fn,sz)
                   os.unlink(bd+fn)
                os.rmdir(bd)
            zip.close()
            print "... done."
            os.rmdir(loaded+rn)

#nahraje games z FROZEN souboru nebo z CLRmamepro datafilez
def initstate(verbose=1):
    path=DATAS[:]
    path.append("config.py")
    latest=0
    reload=0
    for file in path:
        try:
            lm=os.stat(file)[8]
        except OSError:
            reload=1
            if verbose:
               print "Can not check last modification time for file",file
            break
        if latest<lm:
            latest=lm
    try:
        if os.stat(DATAFROZEN)[8]<latest:
            reload=1
    except OSError:
            reload=1

    games={}
    if reload==0:
        try:
            f=open(DATAFROZEN,'r')
            if verbose:
               print "Loading frozen state..."
            games=cPickle.load(f)
            f.close()
            if verbose:
               print "... loaded"
        except IOError:
            if verbose:
               print "... failed"
            reload=1
        except SystemError:
            if verbose:
               print "... failed"
            reload=1

    if reload==1:
        if verbose:
           print "Reparsing datafiles..."
        for fn in DATAS:
            games=readinfo(fn,games)
        if verbose:
           print "Saving frozen state..."
        f=open(DATAFROZEN,'w')
        cPickle.dump(games,f)
        f.close()
        if verbose:
           print "Saved."

    for fn in VERIFY:
        games=readverify(fn,games)
   
    return games

execfile("config.py",globals())
if __name__ == "__main__":
    games=initstate()
    verifybadroms(ROMPATHS,games)

#udela verifikaci a opravi romky
def repair():
    games=initstate()
    verifybadroms(ROMPATHS,games)
    repairbadroms(ROMPATHS,LOADED,games,WANTMERGE,FIXED)
