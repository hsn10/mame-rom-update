class game:
    PERFECT=3   # romset is perfect
    BESTAVAIL=2 # best available romset
    RENAME=1    # some filename problems, otherwise good
    UNKNOWN=0   # state of this romset is unknown
    WRONGZIP=-1 # files are in wrong (clone) zip archive  /notyetused/
    BAD=-2      # romset has some missing files
    CORRUPTED=-3# zip file is corrupted
    MISSING=-4  # zip file is missing

    def __init__(self,gamename,zcloneof=None):
        self.name=gamename
        self.cloneof=zcloneof
        self.good=game.UNKNOWN # 1 yes, 0 unknown, -1 bad, -2 missing or corrupted zip
        self.roms=[]
        self.needmerge=0

    def __repr__(self):
        x='Game '+self.name
        if self.cloneof!=None:
            x+=' clone of '+self.cloneof
        if self.good==game.PERFECT: x+=' (perfect)'
        elif self.good==game.BESTAVAIL: x+= ' (bestavail)'
	elif self.good==game.RENAME: x+= ' (romnames)'
	elif self.good==game.UNKNOWN: x+= ' (unknown)'
	elif self.good==game.WRONGZIP: x+= ' (displaced zip)'
	elif self.good==game.BAD: x+= ' (bad)'
	elif self.good==game.CORRUPTED: x+= ' (corrupted)'
        elif self.good==game.MISSING: x+= ' (missing)'
        else:
	    x+=' (UNKNOWN-STATE!)'

        return x
