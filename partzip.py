#! /usr/bin/python
# This file is (c) Radim Kolar, Licensed as LGPL

import urllib
import httplib
from string import index,lower,rfind,find
import struct
import zlib
import socket
import re

#partial file from zip download

stringCentralFile="PK\001\002"
fmtCentralFile='<4x6xH4xiiiH12xi'
stringLocalFile="PK\003\004"
fmtLocalFile='<4x4xH4x4xiiHH'

class partzip:

    def __init__(self,zurl,zipcache=None,hdrs=[]):
        self.url=zurl
        self.ctsize=-1
        self.direct=None
	self.headers=hdrs
	self.cache=zipcache
	
	#try to load directory from cache if available
	if self.cache:
	   if self.cache.isCached(self.url):
	      junk,junk,self.ctsize,self.direct=self.cache.getinfo(self.url)
	      return

    #vstup: lokalni zip hlavicka + data
    #vystup: vraci delku ktera se musi preskocit (LocalHeaderData) abychom
    #        se dostali na zacatek dat
    def checkLocalHeader(self,data):
        if len(data)<30:
            return None # too few data
        if data[0:4]!=stringLocalFile:
            return None # no magic

        #entry format: 
	# 0           1                 2               3                 4
	#method, compressed size, uncompressed size, filename length, extra field length
        entry=struct.unpack(fmtLocalFile,data[0:30])
        #print "ZIP local header",entry
        skip=entry[3]+entry[4]+30
        return skip

    #nacte centralni adresar zip souboru
    def getdirectory(self,dirsize=20000):
        if self.direct!=None:
            return
        self.size()
	if(self.ctsize<=0): return
	if(dirsize>self.ctsize): dirsize=self.ctsize
        self.direct=self.part(self.ctsize-dirsize,dirsize)
	#test if this is a  zipfile
	if self.direct!=None:
	  if find(self.direct,stringCentralFile) == -1:
	     print "File doesn't looks like Zip Archive"
	     return
	else:
	     return
        #read  more  data  until  fully  satisfied
	while find(self.direct,stringCentralFile) < 100 and dirsize<self.ctsize:
	     print "Possible truncated Central Zip directory, sz=",dirsize
	     print "Reading more data"
	     need=dirsize
	     if need+dirsize>self.ctsize:  need=self.ctsize-dirsize;
	     self.direct=self.direct+self.part(self.ctsize-dirsize-need,need)
	     dirsize+=need

	#save data to cache
	if self.direct and self.cache:
	      self.cache.setinfo(self.url,0,self.ctsize,self.direct)

#vraci binarni dekomprimovana data souboru ze zipace ; HIGH-LEVEL fce
    def getfile(self,name,fsize=0,fcrc=0):
	#load central directory, if not yet loaded
        self.getdirectory()

	if self.direct==None:
	   return None

        method,filename,offset,size=self.centraldirscan(name,fsize,fcrc)

        if method==None:
            print "File",name,"not found in",self.url
            return None
        print "File",filename,"located at offset",offset,"with compr. size",size

        mysize=size+30+len(filename)*2+46+22
	#download data
        data=self.part(offset,mysize)
        if data==None or len(data)<mysize:
            print "Data truncated, network err?"
            return None
        #how much to skip for localheader data?
        skip=self.checkLocalHeader(data)
        if skip==None:
            print "Invalid local Zip file header. Broken zip archive?"
            return None

        #downloaded enough file data?
        if(size+skip>mysize):
            #mysize=size+skip
            #repeat download
	    print "Localheader is bigger than expected, reading more data."
            data+=self.part(offset+mysize,size+skip-mysize)

        #truncate local header + extra data
	data=data[skip:skip+size]
        #decompress data
        if method==8:
            return zlib.decompress(data+'Z',-15)
	elif method==0:
	    return data
	else:
	    print "Unsupported zip compression method",method
	    return None

#scanuje centralni directory
#if size>0 & crc=0 searches by filesize
#if size=0 & crc=0 searches by filename
#if size>0 & crc>0 searches by both filesize and CRC
#vraci komprimacni metodu,filename,offset lokalni hlavicky,delku kompr. dat
    def centraldirscan(self,name,size=0,crc=0):
        #1) find file in central directory
        ofs=-1
        sz=struct.calcsize(fmtCentralFile)
        try:
            while 1:
                ofs+=1
                ofs=index(self.direct,stringCentralFile,ofs)
                entry=struct.unpack(fmtCentralFile,self.direct[ofs:ofs+sz])
                #format entry
                #  0     1       2               3                4      5
                #methoda,crc,compressed_size,decompresed_size,name_size,local_hdroffset
                ename=self.direct[ofs+sz:ofs+sz+entry[4]]
                #test if CRC/SIZE matches
                if size>0:
                    if entry[3]==size:
                        if crc!=0:
                            if entry[1]==crc:
                                return (entry[0],ename,entry[5],entry[2])
                        else:
                            return (entry[0],ename,entry[5],entry[2])
                else:
                    if rfind(lower(ename),lower(name))>-1:
                        return (entry[0],ename,entry[5],entry[2])

        except ValueError:
            return (None,None,None,None)
	    
    #############################
    # HTTP - Specificke operace #
    #############################
    
    #zjisti content-size celeho zipu, nastavi self.ctsize
    #vraci -1 pri chybe
    def size(self):
        if self.ctsize>-1:
            return self.ctsize

        proto,host=urllib.splittype(self.url)
        host,path=urllib.splithost(host)
	try:
	    ht=httplib.HTTP(host)
	    ht.putrequest('GET',path) #HEAD was there
	    ht.putheader('Host',host)
	    ht.putheader('Accept','*/*')
	    if len(self.headers)>0:
		for z in self.headers:
		  a,b=z
		  ht.putheader(a,b)
	    ht.endheaders()
	    errcode, errmsg, headers=ht.getreply()
	    ht.close()
	except socket.error:
	    self.ctsize=-1
	    return -1;

        if errcode!=200:
            self.ctsize=-1
	    print "Got unexpected http rc",errcode
            return -1

        sz=headers.getheader('content-length')
        if sz==None:
            self.ctsize=0
        else:
            self.ctsize=int(sz,10)

        return self.ctsize

    #stahne cast souboru ze serveru
    def part(self,start=0,length=0):
        if start+length>=self.ctsize:
            length=0
        protocol,host=urllib.splittype(self.url)
        host,path=urllib.splithost(host)
        ht=httplib.HTTP(host)
        ht.putrequest('GET',path)
        ht.putheader('Host',host)
        ht.putheader('Accept','*/*')
	if len(self.headers)>0:
	    for z in self.headers:
	      a,b=z
	      ht.putheader(a,b)
        if start>0 or length>0:
            #send range header
            rg='bytes='+repr(start)+'-'
            if length>0:
                rg+=repr(start+length-1)
            ht.putheader('Range',rg)
        ht.endheaders()
        errcode, errmsg, headers=ht.getreply()
	if errcode==200:
	    if (start>0 or length>0): 
		print "Server do not supports resume"
		return None
        elif errcode!=206:
	    print "Got unexpected http rc",errcode
            return None
        return ht.getfile().read()

class zipcache:
    
    def __init__(self):
        self.clean()
	self.dirty=0

    def clean(self):
	self.urls={}
	self.lastmod=[]
	self.size=[]
	self.directory=[]
	self.version=self.myversion()
	self.dirty=1
    
    def checkVersion(self):
        if self.version!=self.myversion():
	   self.clean()
	else:
	   self.dirty=0

    def myversion(self):
        return 1
    
    #Vraci URL,lastmod,velikost,zipdirectory
    def getinfo(self,url):
       if not self.isCached(url):
          return None
       idx=self.urls[url]
       return (url,self.lastmod[idx],self.size[idx],self.directory[idx])
       
    # odstrani URL z cache     
    def removerx(self,mask):
        rx=re.compile(mask)
        for k in self.urls.keys():
	   if  rx.match(k):
	       self.remove(k)
	
    # odstrani URL z cache     
    def remove(self,url):
        if not self.isCached(url): return #nothing to remove
	idx=self.urls[url]
	print "removing",url,"at index",idx
	del self.urls[url]
	del self.lastmod[idx]
	del self.size[idx]
	del self.directory[idx]
	for k in self.urls.keys():
	   if self.urls[k]>idx:
	      self.urls[k]=self.urls[k]-1
        self.dirty=1
	
    def setinfo(self,url,nlastmod,nsize,ndirectory):
       if self.isCached(url):
          idx=self.urls[url]
	  self.lastmod[idx]=nlastmod
	  self.size[idx]=nsize
	  self.directory[idx]=ndirectory
       else:
          idx=len(self.urls)
	  self.urls[url]=idx

	  self.lastmod.append(nlastmod)
	  self.size.append(nsize)
	  self.directory.append(ndirectory)
       
       self.dirty=1
  
    def isCached(self,url):
        if self.urls.has_key(url):
            return 1
	else:
	    return 0
	    
#pz=partzip('http://localhost/~hsn/shootbul.zip')
#filedata=pz.getfile('7f.rom')
#print "Data length=",len(filedata)
