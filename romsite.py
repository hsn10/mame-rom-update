# Generic service for downloading ROMs from HTTP web sites
# Feel free to edit this as you want

import base64
import httplib
import re
import mamedk

# locates game on the specified site.
# returns  [   ( URL, (header,content), ... )  , ... ]

def get(gamename,sitename,LOGIN=None):
   if sitename=='mamefans':
      return [ ('http://mamefans.metropoliglobal.com/down/mame/'+gamename+'.zip',) ];
   elif sitename=='emuchina':
      return [ ('http://www.emuchina.net', basiclogin("emuchina:emuchina"),) ];
   elif sitename=='aceroms':
      return [ ('http://212.43.221.244/superpouet/MAME/roms/'+gamename+'.zip',) ];
   elif sitename=='sys2064':
        getsys2064fixfiles()
	if len(fixfilecache)>0:
	      result=[]
	      for url in fixfilecache:
	        result.append( (url,("Referer","/fixfiles.htm"),("User-Agent", 'Mozilla/3.0 (X11; U; Linux 1.2.13 i386)')))
	      return result
	else:
	      return None
   elif sitename=='mamedk':
        url=mamedk.get(gamename,LOGIN)
	if url is not None:
	      return [ (url, ("Cookie",mamedk.getlogincookie(LOGIN)) ) ]
	else:
	      return None
   
   return None

def basiclogin(LOGIN):
  if LOGIN==None:
     LOGIN='user:unknown';
  return ('Authorization','Basic %s' % base64.encodestring(LOGIN).strip())

fixfilecache=[]
def getsys2064fixfiles():
    global fixfilecache
    
    if len(fixfilecache)>0:
       return

    host='www.sys2064.com'
    url='/fixfiles.htm'
    search='fixes/[^">]+'

    print "Locating available fixfiles on SYS 2064"
    ht=httplib.HTTP(host)
    ht.putrequest('GET',url)
    ht.putheader('Host',host)
    ht.putheader('Accept','*/*')
    ht.endheaders()
    
    reply,msg,hdrs = ht.getreply()
    if reply!=200: return None
    f=ht.getfile()
    result=re.findall(search,f.read())
    fixfilecache=[]
    for uri in result:
      fixfilecache.append("http://"+host+"/"+uri)
    print "Done.",len(fixfilecache),"fixfiles found."
    return fixfilecache

    
if __name__ == "__main__":
    print getsys2064fixfiles()
