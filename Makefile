#GNU Makefile for Mame Rom Update

VERSION=13a

DOC=README.TXT TODO.TXT HISTORY.TXT FAQ.TXT
SOURCES=config.py  loader.py  mamedk.py  partzip.py  repair.py  verify.py \
	game.py romsite.py irc.py genmerge.py
DISTRIB=$(SOURCES) $(DOC) Makefile

default: tags

zip: mru0$(VERSION).zip

mru0$(VERSION).zip: $(DISTRIB)
	-rm -f mru0*
	zip -o -9 mru0$(VERSION) $(DISTRIB) 

install: mru0$(VERSION).zip
	mv mru0$(VERSION).zip ${HOME}/public_html/download/

tags: $(SOURCES)
	-ctags -w $(SOURCES)

clean:
	rm -f tags *.pyc mru0* 

.PHONY: clean zip default install
